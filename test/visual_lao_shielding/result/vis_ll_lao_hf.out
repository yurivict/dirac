DIRAC serial starts by allocating 64000000 words (    488.28 MB -      0.477 GB)
 of memory    out of the allowed maximum of 2147483648 words (  16384.00 MB -     16.000 GB)

Note: maximum allocatable memory for serial run can be set by pam --aw/--ag

 *******************************************************************************
 *                                                                             *
 *                                O U T P U T                                  *
 *                                   from                                      *
 *                                                                             *
 *                   @@@@@    @@   @@@@@     @@@@     @@@@@                    *
 *                   @@  @@        @@  @@   @@  @@   @@                        *
 *                   @@  @@   @@   @@@@@    @@@@@@   @@                        *
 *                   @@  @@   @@   @@ @@    @@  @@   @@                        *
 *                   @@@@@    @@   @@  @@   @@  @@    @@@@@                    *
 *                                                                             *
 *                                                                             *
 %}ZS)S?$=$)]S?$%%>SS$%S$ZZ6cHHMHHHHHHHHMHHM&MHbHH6$L/:<S///</:|/:|:/::!:.::--:%
 $?S$$%$$$$?%?$(SSS$SSSHMMMMMMMMMMMMMMMMMM6H&6S&SH&&k?6$r~::://///::::::.:::-::$
 (%?)Z??$$$(S%$>$)S6HMMMMMMMMMMMMMMMMMMMMMMR6M]&&$6HR$&6(i::::::|i|:::::::-:-::(
 $S?$$)$?$%?))?S/]#MMMMMMMMMMMMMMMMMMMMMMMMMMHM1HRH9R&$$$|):?:/://|:/::/:/.::.:$
 SS$%%?$%((S)?Z[6MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM&HF$$&/)S?<~::!!:::::::/:-:|.S
 SS%%%%S$%%%$$MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHHHHHHM>?/S/:/:::`:/://:/::-::S
 ?$SSSS?%SS$)MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM/4?:S:/:::/:::/:/:::.::?
 S$(S?S$%(?$HMMMMMMMMMMMMMMMMM#&7RH99MMMMMMMMMMMMMMMMMMHHHd$/:::::/::::::-//.:.S
 (?SS(%)S&HMMMMMMMMMMMMMMMMM#S|///???$9HHMMMMMMMMMDSZ&1S/?</>?~:///::|/!:/-:-:.(
 $S?%?<H(MMMMMMMMMMMMMMMMR?`. :.:`::<>:``?/*?##*)$:/>       `((%://::/:::::/::/$
 S$($$)HdMMMMMMMMMMMMMMMP: . `   `  `    `      `-            `Z<:>?::/:::::|:iS
 c%%%&HMMMMMMMMMMMMMMMM6:                                      `$%)>%%</>!:::::c
 S?%/MMMMMMMMMMMMMMMMMMH-                                        /ZSS>?:?~:;/::S
 $SZ?MMMMMMMMMMMMMMMMMH?.                                        \"&((/?//?|:::$
 $%$%&MMMMMMMMMMMMMMMMM:.                                          ?%/S:</::/::$
 ($?}&HMMMMMMMMMMMMMMMM>:                                          $%%<?/i:|i::(
 Z$($&MMMMMMMMMMMMMMMMHk(:.  . -                                   .S/\?\?/!:/:Z
 (??$<HMMMMMMMMMMMMMMMFk|:   -.-.                                  :%/%/(:/:ii|(
 SZ(S?]MMMMMMMMMMMMMMHS?:- -  ::.:                                  |/S:</::?||S
 $%)$$(MMMMMMMMMMMMMMR):`:. :.:::`,,/bcokb/_                       :S?%?|~:/:/:$
 %$$%$)[[?$?MMMMMMMMMM: :.:-.::::$7?<&7&MMMMMMM#/           _ .. ..:</?:(:/::::%
 $$$?Z?HHH~|/MMMMMMMMM/`.-.:.:/:%%%%?dHMMMMMMMMMMH?,-   .,bMMMM6//./i~/~:<:::/:$
 $($S$M//::S?ZHMMMMMH/:.`:::.:/%S/&MMHMMMMMMMMRM&><   ,HMMMMMMMF  :::?:///:|:::$
 )[$S$S($|_i:#>::*H&?/::.::/:\"://:?>>`:&HMHSMMMM$:`-   MMHMMMMHHT .)i/?////::/)
 $$[$$>$}:dHH&$$--?S::-:.:::--/-:``./::>%Zi?)&/?`:.`   `H?$T*\" `  /%?>%:)://ii$
 $&=&/ZS}$RF<:?/-.|%r/:::/:/:`.-.-..|::S//!`\"``          >??:    `SS<S:)!/////$
 Z&]>b[Z(Z?&%:::../S$$:>:::i`.`. `-.`  `                         ,>%%%:>/>/!|:/Z
 $$&/F&1$c$?>:>?/,>?$$ZS/::/:-: ...                              |S?S)S?<~:::::$
 &$&$&$k&>>|?<:<?((/$[/?)i~/:/. - -                              S?:%%%?/:::/::&
 =[/Z[[Fp[MMH$>?Z&S$$$/$S///||..-           -.-                  /((S$:%<:///:/=
 $&>1MHHMMMM6M9MMMM$Z$}$S%/:::.`.            .:/,,,dcb>/:.       ((SSSS%:)!//i|$
 MMMMMMMMMMMR&&RRRHR&&($(?:|i::-             .:%&S&$[&H&``     ../>%;/?>??:<::>M
 MMMMMMMMMMMMS/}S$&&H&[$SS//:::.:.   . . .v</Jq1&&D]&M&<,      :/::/?%%)S>?://:M
 MMMMMMMMMMMM?}$/$$kMM&&$(%/?//:..`.  .|//1d/`://?*/*/\"` `     .:/(SS$%(S%)):%M
 MMMMMMMMMMMM(}$$>&&MMHR#$S%%:?::.:|-.`:;&&b/D/$p=qpv//b/~`   :/~~%%??$=$)Z$S+;M
 MMMMMMMMMMMM[|S$$Z1]MMMMD[$?$:>)/::: :/?:``???bD&{b<<-`     .,:/)|SS(}Z/$$?/<SM
 MMMMMMMMMMMM||$)&7k&MMMMH9]$$??Z%|!/:i::`  `` .             SS?SS?Z/]1$/&$c/$SM
 MMMMMMMMMMMM| -?>[&]HMMMMMMMH1[/7SS(?:/..-` ::/Sc,/_,     _<$?SS%$S/&c&&$&>//<M
 MMMMMMMMMMMMR  `$&&&HMM9MMMMMMM&&c$%%:/:/:.:.:/\?\?/\    _MMHk/7S/]dq&1S<&&></M
 MMMMMMMMMMMMM?  :&96MHMMMMMMMMMMMHHk[S%(<<:// `         ,MMMMMMM&/Z6H]DkH]1$&&M
 MMMMMMMMMMMMMD    99H9HMMMMMMMMMMMMMMMb&%$<:i.:....    .MMMMMMMMM6HHHRH&H&H1SFM
 MMMMMMMMMMMMMM|   `?HMMMMMMMMMMMMMMMMMMMHk6k&>$&Z$/?_.bHMMMMMMMMMMM&6HRM9H6]ZkM
 MMMMMMMMMMMMMMM/    `TMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHMH6RH&R6&M
 MMMMMMMMMMMMMMMM    -|?HMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMFHH6HMD&&M
 MMMMMMMMMMMMMMMMk  ..:~?9MMMMMMMMMMMMM#`:MMMMMMMMMMMMMMMMMMMMMMMMMMMMM9MHkR6&FM
 MMMMMMMMMMMMMMMMM/  .-!:%$ZHMMMMMMMMMR` dMMMMMMMMMMMMMMMMMMMMMMMMMMMMM9MRMHH9&M
 MMMMMMMMMMMMMMMMMML,:.-|::/?&&MMMMMM` .MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHRMH&&6M
 MMMMMMMMMMMMMMMMMMMc%>/:::i<:SMMMMMMHdMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHHM&969kM
 MMMMMMMMMMMMMMMMMMMMSS/$$/(|HMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHH&HH&M
 MMMMMMMMMMMMMMMMMMMM6S/?/MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMR96H1DR1M
 MMMMMMMMMMMMMMMMMMMMM&$MHMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHMH691&&M
 MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMH&R&9ZM
 MMMMMMMMMMMMMMMMMMMMMMMMMRHMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMH&96][6M
 MMMMMMMMMMMMMMMMMMMMMMMMp?:MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM96HH1][FM
 MMMMMMMMMMMMMMMMMMMMMMMM> -HMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMH&1k&$&M
 *******************************************************************************
 *                                                                             *
 *         =========================================================           *
 *                     Program for Atomic and Molecular                        *
 *          Direct Iterative Relativistic All-electron Calculations            *
 *         =========================================================           *
 *                                                                             *
 *                                                                             *
 *    Written by:                                                              *
 *                                                                             *
 *    Trond Saue               Universite Toulouse III           France        *
 *    Lucas Visscher           Vrije Universiteit Amsterdam      Netherlands   *
 *    Hans Joergen Aa. Jensen  University of Southern Denmark    Denmark       *
 *    Radovan Bast             UiT The Arctic University of      Norway        *
 *    Andre S. P. Gomes        CNRS/Universite de Lille          France        *
 *                                                                             *
 *    with contributions from:                                                 *
 *                                                                             *
 *    Ignacio Agustin Aucar    CONICET/Northeastern University   Argentina     *
 *    Vebjoern Bakken          University of Oslo                Norway        *
 *    Chima Chibueze           Vrije Universiteit Amsterdam      Netherlands   *
 *    Joel Creutzberg          University of Southern Denmark    Denmark       *
 *    Kenneth G. Dyall         Schrodinger, Inc., Portland       USA           *
 *    Sebastien Dubillard      University of Strasbourg          France        *
 *    Ulf Ekstroem             University of Oslo                Norway        *
 *    Ephraim Eliav            University of Tel Aviv            Israel        *
 *    Thomas Enevoldsen        University of Southern Denmark    Denmark       *
 *    Elke Fasshauer           University of Tubingen            Germany       *
 *    Timo Fleig               Universite Toulouse III           France        *
 *    Olav Fossgaard           UiT The Arctic University of      Norway        *
 *    Loic Halbert             Universite de Lille               France        *
 *    Erik D. Hedegaard        University of Southern Denmark    Denmark       *
 *    Trygve Helgaker          University of Oslo                Norway        *
 *    Benjamin Helmich-Paris   Max Planck Institute f. Coal Res. Germany       *
 *    Johan Henriksson         Linkoeping University             Sweden        *
 *    Martin van Horn          Universite Toulouse III           France        *
 *    Miroslav Ilias           Matej Bel University              Slovakia      *
 *    Christoph R. Jacob       TU Braunschweig                   Germany       *
 *    Stefan Knecht            GSI Darmstadt/JGU Mainz           Germany       *
 *    Stanislav Komorovsky     Slovak Academy of Sciences        Slovakia      *
 *    Ossama Kullie            University of Kassel              Germany       *
 *    Jon K. Laerdahl          University of Oslo                Norway        *
 *    Christoffer V. Larsen    University of Southern Denmark    Denmark       *
 *    Yoon Sup Lee             KAIST, Daejeon                    South Korea   *
 *    Nanna Holmgaard List     Stockholm Inst. of Technology     Sweden        *
 *    Huliyar S. Nataraj       BME/Budapest Univ. Tech. & Econ.  Hungary       *
 *    Malaya Kumar Nayak       Bhabha Atomic Research Centre     India         *
 *    Patrick Norman           Stockholm Inst. of Technology     Sweden        *
 *    Malgorzata Olejniczak    University of Warsaw              Poland        *
 *    Jeppe Olsen              Aarhus University                 Denmark       *
 *    Jogvan Magnus H. Olsen   University of Southern Denmark    Denmark       *
 *    Anastasios Papadopoulos  Max Planck Institute f. Coal Res. Germany       *
 *    Young Choon Park         KAIST, Daejeon                    South Korea   *
 *    Jesper K. Pedersen       University of Southern Denmark    Denmark       *
 *    Markus Pernpointner      University of Heidelberg          Germany       *
 *    Johann V. Pototschnig    Technical University Graz         Austria       *
 *    Roberto Di Remigio       EuroCC National Competence Centre Sweden        *
 *    Michal Repisky           UiT The Arctic University of      Norway        *
 *    Kenneth Ruud             UiT The Arctic University of      Norway        *
 *    Pawel Salek              Stockholm Inst. of Technology     Sweden        *
 *    Bernd Schimmelpfennig    Karlsruhe Institute of Technology Germany       *
 *    Bruno Senjean            CNRS/Universite de Montpellier    France        *
 *    Avijit Shee              University of Berkeley            USA           *
 *    Jetze Sikkema            Vrije Universiteit Amsterdam      Netherlands   *
 *    Ayaki Sunaga             Kyoto University                  Japan         *
 *    Andreas J. Thorvaldsen   UiT The Arctic University of      Norway        *
 *    Joern Thyssen            University of Southern Denmark    Denmark       *
 *    Joost N. P. van Stralen  Vrije Universiteit Amsterdam      Netherlands   *
 *    Marta L. Vidal           Cardiff University                UK            *
 *    Sebastien Villaume       Linkoeping University             Sweden        *
 *    Olivier Visser           University of Groningen           Netherlands   *
 *    Toke Winther             University of Southern Denmark    Denmark       *
 *    Shigeyoshi Yamamoto      Chukyo University                 Japan         *
 *    Joel Creutzberg          Lund University                   Sweden        *
 *    Xiang Yuan               Universite de Lille               France        *
 *                                                                             *
 *    For more information about the DIRAC code see http://diracprogram.org    *
 *                                                                             *
 *    This is an experimental code. The authors accept no responsibility       *
 *    for the performance of the code or for the correctness of the results.   *
 *                                                                             *
 *    This program is free software; you can redistribute it and/or            *
 *    modify it under the terms of the GNU Lesser General Public               *
 *    License version 2.1 as published by the Free Software Foundation.        *
 *                                                                             *
 *    If results obtained with this code are published, an                     *
 *    appropriate citation would be:                                           *
 *                                                                             *
 *    DIRAC, a relativistic ab initio electronic structure program,            *
 *    Release DIRAC22 (2022), written by                                       *
 *    T. Saue, L. Visscher, H. J. Aa. Jensen, R. Bast, and A. S. P. Gomes      *
 *    with contributions from I. A. Aucar, V. Bakken, C. Chibueze,             *
 *    J. Creutzberg, K. G. Dyall, S. Dubillard, U. Ekstroem, E. Eliav,         *
 *    T. Enevoldsen, E. Fasshauer, T. Fleig, O. Fossgaard, L. Halbert,         *
 *    E. D. Hedegaard, T. Helgaker, J. Henriksson, M. van Horn, M. Ilias,      *
 *    Ch. R. Jacob, S. Knecht, S. Komorovsky, O. Kullie, J. K. Laerdahl,       *
 *    C. V. Larsen, Y. S. Lee, N. H. List, H. S. Nataraj, M. K. Nayak,         *
 *    P. Norman, M. Olejniczak, J. Olsen, J. M. H. Olsen, A. Papadopoulos,     *
 *    Y. C. Park, J. K. Pedersen, M. Pernpointner, J. V. Pototschnig,          *
 *    R. Di Remigio, M. Repisky, K. Ruud, P. Salek, B. Schimmelpfennig,        *
 *    B. Senjean, A. Shee, J. Sikkema, A. Sunaga, A. J. Thorvaldsen,           *
 *    J. Thyssen, J. N. P. van Stralen, M. L. Vidal, S. Villaume,              *
 *    O. Visser, T. Winther, S. Yamamoto and X. Yuan                           *
 *    (see http://diracprogram.org).                                           *
 *                                                                             *
 *    as well as our reference paper: J. Chem. Phys. 152 (2020) 204104.        *
 *                                                                             *
 *******************************************************************************


Version information
-------------------

Branch        | gosia/visual
Commit hash   | 3aa13b775
Commit author | gosia olejniczak
Commit date   | Tue Mar 8 10:08:14 2022 +0100


Configuration and build information
-----------------------------------

Who compiled             | gosia
Compiled on server       | nela
Operating system         | Linux-5.4.0-100-generic
CMake version            | 3.23.0-rc2
CMake generator          | Unix Makefiles
CMake build type         | debug
Configuration time       | 2022-03-09 10:28:28.730630
Python version           | 3.8.1
Fortran compiler         | /usr/bin/gfortran-8
Fortran compiler version | 8.4.0
Fortran compiler flags   |  -g -fcray-pointer -fbacktrace -fno-range-check -DVAR_GFORTRAN -DVAR_MFDS -fopenmp -g -pg -g -fcray-pointer -fbacktrace -fno-range-check -DVAR_GFORTRAN -DVAR_MFDS -fopenmp -g -pg
C compiler               | /usr/bin/gcc-8
C compiler version       | 8.4.0
C compiler flags         |  -g -fopenmp -g -pg -g -fopenmp -g -pg
C++ compiler             | /usr/bin/g++
C++ compiler version     | 9.4.0
C++ compiler flags       |  -g -Wall -Wno-unknown-pragmas -Wno-sign-compare -Woverloaded-virtual -Wwrite-strings -Wno-unused -fopenmp -g -pg -g -Wall -Wno-unknown-pragmas -Wno-sign-compare -Woverloaded-virtual -Wwrite-strings -Wno-unused -fopenmp -g -pg
Static linking           | False
64-bit integers          | False
MPI parallelization      | False
MPI launcher             | unknown
Math libraries           | /usr/lib/x86_64-linux-gnu/libatlas.so;/usr/lib/x86_64-linux-gnu/atlas/liblapack.so;/usr/lib/x86_64-linux-gnu/libf77blas.so;/usr/lib/x86_64-linux-gnu/libcblas.so;/usr/lib/x86_64-linux-gnu/libatlas.so
Builtin BLAS library     | OFF
Builtin LAPACK library   | OFF
Explicit libraries       | unknown
Compile definitions      | HAVE_OPENMP;SYS_LINUX;PRG_DIRAC;INSTALL_WRKMEM=64000000;HAS_PCMSOLVER;EXA_TALSH_ONLY;BUILD_GEN1INT;HAS_PELIB;MOD_QCORR;HAS_STIELTJES;MOD_LAO_REARRANGED;MOD_MCSCF_spinfree;MOD_AOOSOC;MOD_ESR;MOD_KRCC;MOD_SRDFT;HAS_LAPLACE;MOD_EXACORR

EXACORR dependencies
--------------------
Exatensor source repo    | https://gitlab.com/DmitryLyakh/ExaTensor.git
Exatensor git hash       | e8e6f6351fabad514be9dd07d3c0a3a9e81c3f52
Exatensor configuration  | WRAP=NOWRAP BUILD_TYPE=OPT EXA_TALSH_ONLY=YES CMAKE_Fortran_COMPILER=/usr/bin/gfortran-8 CMAKE_C_COMPILER=/usr/bin/gcc-8 CMAKE_CXX_COMPILER=/usr/bin/g++ TOOLKIT=GNU EXA_OS=LINUX GPU_CUDA=NOCUDA  BLASLIB=ATLAS PATH_BLAS_ATLAS=/usr/lib/x86_64-linux-gnu


 LAPACK integer*4/8 selftest passed
 Selftest of ISO_C_BINDING Fortran - C/C++ interoperability PASSED


 * openMP activated,  thread limit - # processes - # threads: *****    4    1

Execution time and host
-----------------------

 
     Date and time (Linux)  : Wed Mar  9 12:23:39 2022
     Host name              : nela                                    

 * Opening HDF5 checkpoint file 
    - New checkpoint file created


Contents of the input file
--------------------------

**DIRAC                                                                                             
**HAMILTONIAN                                                                                       
.LEVY-LEBLOND                                                                                       
.GAUGEO                                                                                             
 5.0 5.0 5.0                                                                                        
**INTEGRALS                                                                                         
*READIN                                                                                             
.CONTRACT                                                                                           
**WAVE FUNCTION                                                                                     
.SCF                                                                                                
**VISUAL                                                                                            
.NDIPZ                                                                                              
 1 PAMXVC 2                                                                                         
.NDIPZDIA                                                                                           
 1 DFCOEF 0                                                                                         
.LONDON                                                                                             
 Z                                                                                                  
.3D_INTEGRATE                                                                                       
.SCALE                                                                                              
 -53.25135    !this is to get integrated nr in ppm                                                  
*END OF                                                                                             


Contents of the molecule file
-----------------------------

INTGRL                                                                                              
.                                                                                                   
.                                                                                                   
C   2    0                                                                                          
       9.     1                                                                                     
F   0.0 -1.7  0.0                                                                                   
LARGE BASIS 6-31G                                                                                   
       1.     1                                                                                     
H   0.0  0.0  0.0                                                                                   
LARGE BASIS 6-31G                                                                                   
FINISH                                                                                              


    *************************************************************************
    ********************* DIRAC: No title specified !!! *********************
    *************************************************************************



    **************************************************************************
    ************************** General DIRAC set-up **************************
    **************************************************************************

   CODATA Recommended Values of the Fundamental Physical Constants: 2018  
             Peter J. Mohr, David B. Newell and Barry N. Taylor           
         Reviews of Modern Physics, Vol. 93, 025010 (2021)                
 * The speed of light :        137.0359992
 * Running in four-component mode
 * Direct evaluation of the following two-electron integrals:
   - LL-integrals
   - SL-integrals
   - SS-integrals
   - GT-integrals
 * Spherical transformation embedded in MO-transformation
   for large components
 * Transformation to scalar RKB basis embedded in
   MO-transformation for small components
 * Thresholds for linear dependence:
   Large components:   1.00D-06
   Small components:   1.00D-08
 * General print level   :   0


    *************************************************************************
    ****************** Output from HERMIT input processing ******************
    *************************************************************************

 Default print level:        1
 Using default nuclear model: Gaussian charge distribution.

 Two-electron integrals not calculated.


 Ordinary (field-free non-relativistic) Hamiltonian integrals not calculated.



 Changes of defaults for *READIN
 -------------------------------




   ***************************************************************************
   ****************** Output from MOLECULE input processing ******************
   ***************************************************************************



  Title Cards
  -----------

  .                                                                       
  .                                                                       
  Nuclear Gaussian exponent for atom of charge   9.000 :    5.3546903324D+08
  Nuclear Gaussian exponent for atom of charge   1.000 :    2.1248236111D+09


                          SYMGRP:Point group information
                          ------------------------------

Point group: C1 

   * Character table

        |  E 
   -----+-----
    A   |   1

   * Direct product table

        | A  
   -----+-----
    A   | A  


                            **************************
                            *** Output from DBLGRP ***
                            **************************

   * One fermion irrep:   E1 
   * Quaternionic group. NZ = 4
   * Direct product decomposition:
          E1  x E1  : A   + A   + A   + A  


                                 Spinor structure
                                 ----------------


   * Fermion irrep no.: 1
      La  |  A  (1)  A  (1)  |
      Sa  |  A  (1)  A  (1)  |
      Lb  |  A  (1)  A  (1)  |
      Sb  |  A  (1)  A  (1)  |


                              Quaternion symmetries
                              ---------------------

    Rep  T(+)
    -----------------------------
    A    1  i  j  k

  Nuclear repulsion energy                       :      5.294117647059 Hartree

  Nuclear contribution to electric dipole moment :    0.000000000000  -15.300000000000    0.000000000000 a.u.;  origin (0,0,0)


  Atoms and basis sets
  --------------------

  Number of atom types :    2
  Total number of atoms:    2

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  F           1       9      22       9      L  - [10s4p|3s2p]                                                   
                             58      58      S  - [4s10p4d|4s10p4d]                                              
  H           1       1       4       2      L  - [4s|2s]                                                        
                             12      12      S  - [4p|4p]                                                        
  ----------------------------------------------------------------------
                             26      11      L  - large components
                             70      70      S  - small components
  ----------------------------------------------------------------------
  total:      2      10      96      81

  Cartesian basis used.
  Threshold for integrals (to be written to file):  1.00D-15


  References for the basis sets
  -----------------------------

  Atom type   1   2
   Elements                             References                                
   --------                             ----------                                
    H - He:  W.J. Hehre, R. Ditchfield and J.A. Pople, J. Chem. Phys. 56,         
   Li - Ne:  2257 (1972). Note: Li and B come from J.D. Dill and J.A.             
             Pople, J. Chem. Phys. 62, 2921 (1975).                               
   Na - Ar:  M.M. Francl, W.J. Petro, W.J. Hehre, J.S. Binkley, M.S. Gordon,      
             D.J. DeFrees and J.A. Pople, J. Chem. Phys. 77, 3654 (1982)          
   K  - Zn:  V. Rassolov, J.A. Pople, M. Ratner and T.L. Windus, J. Chem. Phys.   
             109, 1223 (1998)                                                     
   **                                                                             


  Cartesian Coordinates (bohr)
  ----------------------------

  Total number of coordinates:  6


   1   F        x      0.0000000000
   2            y     -1.7000000000
   3            z      0.0000000000

   4   H        x      0.0000000000
   5            y      0.0000000000
   6            z      0.0000000000



  Cartesian coordinates in XYZ format (Angstrom)
  ----------------------------------------------

    2

F      0.0000000000  -0.8996012585   0.0000000000
H      0.0000000000   0.0000000000   0.0000000000


   Interatomic separations (in Angstroms):
   ---------------------------------------

            F           H   

   F       0.000000
   H       0.899601    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H          F                              0.899601



   Nuclear repulsion energy                          :    5.294117647059 Hartree
   Nuclear contribution to electric dipole moment    :        0.00000000      -15.30000000        0.00000000 a.u.

                       * Total mass:    20.006228 amu
                       * Natural abundance:  99.985 %


* Center-of-mass coordinates (a.u.):       0.000000000000000  -1.614361542815567   0.000000000000000
* Center-of-mass coordinates (A)   :       0.000000000000000  -0.854283338616206   0.000000000000000


                                GETLAB: AO-labels
                                -----------------

   * Large components:    5
     1  L F   1 s        2  L F   1 px       3  L F   1 py       4  L F   1 pz       5  L H   1 s   
   * Small components:   13
     6  S F   1 s        7  S F   1 px       8  S F   1 py       9  S F   1 pz      10  S F   1 dxx     11  S F   1 dxy 
    12  S F   1 dxz     13  S F   1 dyy     14  S F   1 dyz     15  S F   1 dzz     16  S H   1 px      17  S H   1 py  
    18  S H   1 pz  


  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:           81
  Number of large orbitals in each symmetry:     11
  Number of small orbitals in each symmetry:     70

* Large component functions

  Symmetry  A  ( 1)

        3 functions:    F  s   
        2 functions:    F  px  
        2 functions:    F  py  
        2 functions:    F  pz  
        2 functions:    H  s   

* Small component functions

  Symmetry  A  ( 1)

        4 functions:    F  s   
       10 functions:    F  px  
       10 functions:    F  py  
       10 functions:    F  pz  
        4 functions:    F  dxx 
        4 functions:    F  dxy 
        4 functions:    F  dxz 
        4 functions:    F  dyy 
        4 functions:    F  dyz 
        4 functions:    F  dzz 
        4 functions:    H  px  
        4 functions:    H  py  
        4 functions:    H  pz  


   ***************************************************************************
   *************************** Hamiltonian defined ***************************
   ***************************************************************************


 One-electron operator origins:
 - General operator origin (a.u.)       :   0.000000000000000   0.000000000000000   0.000000000000000
 - Magnetic gauge origin (a.u.)         :   5.000000000000000   5.000000000000000   5.000000000000000
 - Dipole (and multipole) origin (a.u.) :   0.000000000000000   0.000000000000000   0.000000000000000
 * Print level:    0
 * Levy-Leblond Hamiltonian
   - Spin-orbit interactions neglected
 * Default integral flags passed to all modules
   - LL-integrals:     1
   - LS-integrals:     0
   - SS-integrals:     0
   - GT-integrals:     0


 Information about the restricted kinetic balance scheme:
 * Default RKB projection:
   1: Pre-projection in scalar basis
   2: Removal of unphysical solutions (via diagonalization of free particle Hamiltonian)


    **************************************************************************
    ************************** Wave function module **************************
    **************************************************************************

 Wave function types requested (in input order):
     HF        

 Wave function jobs in execution order (expanded):
 * Hartree-Fock calculation

 * Initial Automatic occupation based on:
   Total charge of atoms =  10
   Charge of molecule    =   0
   i.e. no. of electrons =  10
===========================================================================
 *SCF: Set-up for Hartree-Fock calculation:
===========================================================================
 * Number of fermion irreps: 1
 * Closed shell SCF calculation with    10 electrons in    5 orbitals.
 * Sum of atomic potentials used for start guess
 * General print level   :   0

 ***** INITIAL TRIAL SCF FUNCTION *****
 * Trial vectors read from file DFCOEF
 * Scaling of active-active block correction to open shell Fock operator    0.500000
   to improve convergence (default value).

 ***** SCF CONVERGENCE CRITERIA *****
 * Convergence on norm of error vector (gradient).
   Desired convergence:1.000D-07
   Allowed convergence:1.000D-06

 ***** CONVERGENCE CONTROL *****
 * Fock matrix constructed using differential density matrix
    with optimal parameter.
 * DIIS (in MO basis)
 * DIIS will be activated when convergence reaches : 1.00D+20
   - Maximum size of B-matrix:   10
 * Damping of Fock matrix when DIIS is not activated. 
   Weight of old matrix    : 0.250
 * Maximum number of SCF iterations  :   50
 * No quadratic convergent Hartree-Fock
 * Contributions from 2-electron integrals to Fock matrix:
   LL-integrals.
    ---> this is default setting from Hamiltonian input
 * NB!!! No e-p rotations in 2nd order optimization.
 ***** OUTPUT CONTROL *****
 * Only electron eigenvalues written out.


 ********************************************************************************
 *************************** Input consistency checks ***************************
 ********************************************************************************



    *************************************************************************
    ************************ End of input processing ************************
    *************************************************************************



                      Nuclear contribution to dipole moments
                      --------------------------------------

                               au             Debye

                    x      0.00000000      0.00000000
                    y    -15.30000000    -38.88908100
                    z      0.00000000      0.00000000

                        1 Debye =   2.54177000 a.u. 

Total time used in ONEGEN (CPU)  0.00903100s and (WALL)  0.00900000s


                       Generating Lowdin canonical matrix:
                       -----------------------------------

   L   A     * Deleted:          0(Proj:          0, Lindep:          0) Smin: 0.11E+00
   S   A     * Deleted:          4(Proj:          4, Lindep:          0) Smin: 0.17E-04


                                Output from MODHAM
                                ------------------

 * All positronic solutions deleted !


      **********************************************************************
      ************************* Orbital dimensions *************************
      **********************************************************************

No. of positive energy orbitals (NESH):    11
No. of negative energy orbitals (NPSH):     0
Total no. of orbitals           (NORB):    11
 TEST..run_visual

  *** Visualisation module ***
 --------------------------------------------------------------------------------------
 --------------------------------------------------------------------------------------
  Visualisation : DFCOEF file read
 visual: connection matrix (T^B in MO basis) read from TBMO file
 nr_xyz_comp =            3
 component =            3
 irep =            0
 sum over e+p shells =  F
 >>>> time used in plot:   0.00 seconds


                            +--------------------------+
                            ! 3D numerical integration !
                            +--------------------------+



 ********************************************************************************
 ********************** Slater-Bragg atomic size estimates **********************
 ********************************************************************************



                        Slater-Bragg relative atomic sizes
                        ----------------------------------


               Column   1     Column   2
       1       1.00000000     1.42857143
       2       0.70000000     1.00000000
    ==== End of matrix output ====

DFTGRD: ATSIZE not requested or no trial vectors found. Slater-Bragg radii will be used.

Atom  Deg   Rmin        Rmax        Step size     #rp     #tp
=============================================================
F       1   0.739E-06   0.705E+01   0.131E+00     108   25860
H       1   0.143E-04   0.989E+01   0.142E+00      81   24126

   Number of grid points in quadrature:     49986 (100.0%)


 >>>> Time used in DFTGRD:   0.22 seconds


     integrated values in columns: scalar, x-component, y-component, z-component

    0.3869495068E+03    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00

----------------------------------------------------------------------------------------------------


 >>>> time used in integration:   2.10 seconds

 * Closing HDF5 checkpoint file 
    - Required data set /result/wavefunctions/scf/energy is missing
    - Required data set /result/wavefunctions/scf/mobasis/eigenvalues is missing
    - Required data set /result/wavefunctions/scf/mobasis/mobasis_id is missing
    - Required data set /result/wavefunctions/scf/mobasis/n_basis is missing
    - Required data set /result/wavefunctions/scf/mobasis/n_mo is missing
    - Required data set /result/wavefunctions/scf/mobasis/nz is missing
    - Required data set /result/wavefunctions/scf/mobasis/orbitals is missing
    - Checkpoint file does not contains all required data, can not be used for restarts
    - Checkpoint file closed

*****************************************************
********** E N D   of   D I R A C  output  **********
*****************************************************


 
     Date and time (Linux)  : Wed Mar  9 12:23:42 2022
     Host name              : nela                                    

>>>> Node 0, utime: 2, stime: 0, minflt: 44465, majflt: 0, nvcsw: 0, nivcsw: 37, maxrss: 285820
>>>> Total WALL time used in DIRAC: 3s
  
  Dynamical Memory Usage Summary for Master
  
  Mean allocation size (Mb) :        41.58
  
  Largest          10  allocations
  
      488.28 Mb at subroutine __allocator_track_if_MOD_allocator_registe for WORK in PAMSET - 2                        
      488.28 Mb at subroutine __allocator_track_if_MOD_allocator_registe for WORK in GMOTRA                            
      488.28 Mb at subroutine __allocator_track_if_MOD_allocator_registe for WORK in PAMSET - 1                        
      488.28 Mb at subroutine __allocator_track_if_MOD_allocator_registe for test allocation of work array in DIRAC mai
        7.63 Mb at subroutine __allocator_track_if_MOD_allocator_registe for unnamed variable                          
        7.63 Mb at subroutine __allocator_track_if_MOD_allocator_registe for unnamed variable                          
        7.63 Mb at subroutine __allocator_track_if_MOD_allocator_registe for unnamed variable                          
        7.63 Mb at subroutine __allocator_track_if_MOD_allocator_registe for unnamed variable                          
        4.94 Mb at subroutine __allocator_track_if_MOD_allocator_registe for unnamed variable                          
        4.94 Mb at subroutine __allocator_track_if_MOD_allocator_registe for unnamed variable                          
  
  Peak memory usage:       488.33 MB
  Peak memory usage:        0.477 GB
       reached at subroutine : __allocator_track_if_MOD_allocator_registe
              for variable   : buf in butobs                             
  
 MEMGET high-water mark:     0.00 MB

*****************************************************
DIRAC pam run in /home/gosia/devel/dirac/build-gosia-visual-debug-profile/test/visual_lao_shielding
