  ** interface to 32-bit integer MPI enabled **

DIRAC serial starts by allocating 64000000 words (    488.28 MB -      0.477 GB)
 of memory    out of the allowed maximum of 2147483648 words (  16384.00 MB -     16.000 GB)

Note: maximum allocatable memory for serial run can be set by pam --aw/--ag

 *******************************************************************************
 *                                                                             *
 *                                O U T P U T                                  *
 *                                   from                                      *
 *                                                                             *
 *                   @@@@@    @@   @@@@@     @@@@     @@@@@                    *
 *                   @@  @@        @@  @@   @@  @@   @@                        *
 *                   @@  @@   @@   @@@@@    @@@@@@   @@                        *
 *                   @@  @@   @@   @@ @@    @@  @@   @@                        *
 *                   @@@@@    @@   @@  @@   @@  @@    @@@@@                    *
 *                                                                             *
 *                                                                             *
 %}ZS)S?$=$)]S?$%%>SS$%S$ZZ6cHHMHHHHHHHHMHHM&MHbHH6$L/:<S///</:|/:|:/::!:.::--:%
 $?S$$%$$$$?%?$(SSS$SSSHMMMMMMMMMMMMMMMMMM6H&6S&SH&&k?6$r~::://///::::::.:::-::$
 (%?)Z??$$$(S%$>$)S6HMMMMMMMMMMMMMMMMMMMMMMR6M]&&$6HR$&6(i::::::|i|:::::::-:-::(
 $S?$$)$?$%?))?S/]#MMMMMMMMMMMMMMMMMMMMMMMMMMHM1HRH9R&$$$|):?:/://|:/::/:/.::.:$
 SS$%%?$%((S)?Z[6MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM&HF$$&/)S?<~::!!:::::::/:-:|.S
 SS%%%%S$%%%$$MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHHHHHHM>?/S/:/:::`:/://:/::-::S
 ?$SSSS?%SS$)MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM/4?:S:/:::/:::/:/:::.::?
 S$(S?S$%(?$HMMMMMMMMMMMMMMMMM#&7RH99MMMMMMMMMMMMMMMMMMHHHd$/:::::/::::::-//.:.S
 (?SS(%)S&HMMMMMMMMMMMMMMMMM#S|///???$9HHMMMMMMMMMDSZ&1S/?</>?~:///::|/!:/-:-:.(
 $S?%?<H(MMMMMMMMMMMMMMMMR?`. :.:`::<>:``?/*?##*)$:/>       `((%://::/:::::/::/$
 S$($$)HdMMMMMMMMMMMMMMMP: . `   `  `    `      `-            `Z<:>?::/:::::|:iS
 c%%%&HMMMMMMMMMMMMMMMM6:                                      `$%)>%%</>!:::::c
 S?%/MMMMMMMMMMMMMMMMMMH-                                        /ZSS>?:?~:;/::S
 $SZ?MMMMMMMMMMMMMMMMMH?.                                        \"&((/?//?|:::$
 $%$%&MMMMMMMMMMMMMMMMM:.                                          ?%/S:</::/::$
 ($?}&HMMMMMMMMMMMMMMMM>:                                          $%%<?/i:|i::(
 Z$($&MMMMMMMMMMMMMMMMHk(:.  . -                                   .S/\?\?/!:/:Z
 (??$<HMMMMMMMMMMMMMMMFk|:   -.-.                                  :%/%/(:/:ii|(
 SZ(S?]MMMMMMMMMMMMMMHS?:- -  ::.:                                  |/S:</::?||S
 $%)$$(MMMMMMMMMMMMMMR):`:. :.:::`,,/bcokb/_                       :S?%?|~:/:/:$
 %$$%$)[[?$?MMMMMMMMMM: :.:-.::::$7?<&7&MMMMMMM#/           _ .. ..:</?:(:/::::%
 $$$?Z?HHH~|/MMMMMMMMM/`.-.:.:/:%%%%?dHMMMMMMMMMMH?,-   .,bMMMM6//./i~/~:<:::/:$
 $($S$M//::S?ZHMMMMMH/:.`:::.:/%S/&MMHMMMMMMMMRM&><   ,HMMMMMMMF  :::?:///:|:::$
 )[$S$S($|_i:#>::*H&?/::.::/:\"://:?>>`:&HMHSMMMM$:`-   MMHMMMMHHT .)i/?////::/)
 $$[$$>$}:dHH&$$--?S::-:.:::--/-:``./::>%Zi?)&/?`:.`   `H?$T*\" `  /%?>%:)://ii$
 $&=&/ZS}$RF<:?/-.|%r/:::/:/:`.-.-..|::S//!`\"``          >??:    `SS<S:)!/////$
 Z&]>b[Z(Z?&%:::../S$$:>:::i`.`. `-.`  `                         ,>%%%:>/>/!|:/Z
 $$&/F&1$c$?>:>?/,>?$$ZS/::/:-: ...                              |S?S)S?<~:::::$
 &$&$&$k&>>|?<:<?((/$[/?)i~/:/. - -                              S?:%%%?/:::/::&
 =[/Z[[Fp[MMH$>?Z&S$$$/$S///||..-           -.-                  /((S$:%<:///:/=
 $&>1MHHMMMM6M9MMMM$Z$}$S%/:::.`.            .:/,,,dcb>/:.       ((SSSS%:)!//i|$
 MMMMMMMMMMMR&&RRRHR&&($(?:|i::-             .:%&S&$[&H&``     ../>%;/?>??:<::>M
 MMMMMMMMMMMMS/}S$&&H&[$SS//:::.:.   . . .v</Jq1&&D]&M&<,      :/::/?%%)S>?://:M
 MMMMMMMMMMMM?}$/$$kMM&&$(%/?//:..`.  .|//1d/`://?*/*/\"` `     .:/(SS$%(S%)):%M
 MMMMMMMMMMMM(}$$>&&MMHR#$S%%:?::.:|-.`:;&&b/D/$p=qpv//b/~`   :/~~%%??$=$)Z$S+;M
 MMMMMMMMMMMM[|S$$Z1]MMMMD[$?$:>)/::: :/?:``???bD&{b<<-`     .,:/)|SS(}Z/$$?/<SM
 MMMMMMMMMMMM||$)&7k&MMMMH9]$$??Z%|!/:i::`  `` .             SS?SS?Z/]1$/&$c/$SM
 MMMMMMMMMMMM| -?>[&]HMMMMMMMH1[/7SS(?:/..-` ::/Sc,/_,     _<$?SS%$S/&c&&$&>//<M
 MMMMMMMMMMMMR  `$&&&HMM9MMMMMMM&&c$%%:/:/:.:.:/\?\?/\    _MMHk/7S/]dq&1S<&&></M
 MMMMMMMMMMMMM?  :&96MHMMMMMMMMMMMHHk[S%(<<:// `         ,MMMMMMM&/Z6H]DkH]1$&&M
 MMMMMMMMMMMMMD    99H9HMMMMMMMMMMMMMMMb&%$<:i.:....    .MMMMMMMMM6HHHRH&H&H1SFM
 MMMMMMMMMMMMMM|   `?HMMMMMMMMMMMMMMMMMMMHk6k&>$&Z$/?_.bHMMMMMMMMMMM&6HRM9H6]ZkM
 MMMMMMMMMMMMMMM/    `TMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHMH6RH&R6&M
 MMMMMMMMMMMMMMMM    -|?HMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMFHH6HMD&&M
 MMMMMMMMMMMMMMMMk  ..:~?9MMMMMMMMMMMMM#`:MMMMMMMMMMMMMMMMMMMMMMMMMMMMM9MHkR6&FM
 MMMMMMMMMMMMMMMMM/  .-!:%$ZHMMMMMMMMMR` dMMMMMMMMMMMMMMMMMMMMMMMMMMMMM9MRMHH9&M
 MMMMMMMMMMMMMMMMMML,:.-|::/?&&MMMMMM` .MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHRMH&&6M
 MMMMMMMMMMMMMMMMMMMc%>/:::i<:SMMMMMMHdMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHHM&969kM
 MMMMMMMMMMMMMMMMMMMMSS/$$/(|HMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHH&HH&M
 MMMMMMMMMMMMMMMMMMMM6S/?/MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMR96H1DR1M
 MMMMMMMMMMMMMMMMMMMMM&$MHMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMHMH691&&M
 MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMH&R&9ZM
 MMMMMMMMMMMMMMMMMMMMMMMMMRHMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMH&96][6M
 MMMMMMMMMMMMMMMMMMMMMMMMp?:MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM96HH1][FM
 MMMMMMMMMMMMMMMMMMMMMMMM> -HMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMH&1k&$&M
 *******************************************************************************
 *                                                                             *
 *         =========================================================           *
 *                     Program for Atomic and Molecular                        *
 *          Direct Iterative Relativistic All-electron Calculations            *
 *         =========================================================           *
 *                                                                             *
 *                                                                             *
 *    Written by:                                                              *
 *                                                                             *
 *    Radovan Bast             UiT The Arctic University of      Norway        *
 *    Andre S. P. Gomes        CNRS/Universite de Lille          France        *
 *    Trond Saue               Universite Toulouse III           France        *
 *    Lucas Visscher           Vrije Universiteit Amsterdam      Netherlands   *
 *    Hans Joergen Aa. Jensen  University of Southern Denmark    Denmark       *
 *                                                                             *
 *    with contributions from:                                                 *
 *                                                                             *
 *    Ignacio Agustin Aucar    CONICET/Northeastern University   Argentina     *
 *    Vebjoern Bakken          University of Oslo                Norway        *
 *    Kenneth G. Dyall         Schrodinger, Inc., Portland       USA           *
 *    Sebastien Dubillard      University of Strasbourg          France        *
 *    Ulf Ekstroem             University of Oslo                Norway        *
 *    Ephraim Eliav            University of Tel Aviv            Israel        *
 *    Thomas Enevoldsen        University of Southern Denmark    Denmark       *
 *    Elke Fasshauer           University of Aarhus              Denmark       *
 *    Timo Fleig               Universite Toulouse III           France        *
 *    Olav Fossgaard           UiT The Arctic University of      Norway        *
 *    Loic Halbert             Universite de Lille               France        *
 *    Erik D. Hedegaard        University of Southern Denmark    Denmark       *
 *    Trygve Helgaker          University of Oslo                Norway        *
 *    Benjamin Helmich-Paris   Max Planck Institute f. Coal Res. Germany       *
 *    Johan Henriksson         Linkoeping University             Sweden        *
 *    Miroslav Ilias           Matej Bel University              Slovakia      *
 *    Christoph R. Jacob       TU Braunschweig                   Germany       *
 *    Stefan Knecht            GSI Darmstadt/JGU Mainz           Germany       *
 *    Stanislav Komorovsky     Slovak Academy of Sciences        Slovakia      *
 *    Ossama Kullie            University of Kassel              Germany       *
 *    Jon K. Laerdahl          University of Oslo                Norway        *
 *    Christoffer V. Larsen    University of Southern Denmark    Denmark       *
 *    Yoon Sup Lee             KAIST, Daejeon                    South Korea   *
 *    Nanna Holmgaard List     Stanford University               USA           *
 *    Huliyar S. Nataraj       BME/Budapest Univ. Tech. & Econ.  Hungary       *
 *    Malaya Kumar Nayak       Bhabha Atomic Research Centre     India         *
 *    Patrick Norman           Stockholm Inst. of Technology     Sweden        *
 *    Malgorzata Olejniczak    University of Warsaw              Poland        *
 *    Jeppe Olsen              Aarhus University                 Denmark       *
 *    Jogvan Magnus H. Olsen   University of Southern Denmark    Denmark       *
 *    Anastasios Papadopoulos  Max Planck Institute f. Coal Res. Germany       *
 *    Young Choon Park         KAIST, Daejeon                    South Korea   *
 *    Jesper K. Pedersen       University of Southern Denmark    Denmark       *
 *    Markus Pernpointner      University of Heidelberg          Germany       *
 *    Johann V. Pototschnig    Technical University Graz         Austria       *
 *    Roberto Di Remigio       UiT The Arctic University of      Norway        *
 *    Michal Repisky           UiT The Arctic University of      Norway        *
 *    Kenneth Ruud             UiT The Arctic University of      Norway        *
 *    Pawel Salek              Stockholm Inst. of Technology     Sweden        *
 *    Bernd Schimmelpfennig    Karlsruhe Institute of Technology Germany       *
 *    Bruno Senjean            University of Leiden              Netherlands   *
 *    Avijit Shee              University of Michigan            USA           *
 *    Jetze Sikkema            Vrije Universiteit Amsterdam      Netherlands   *
 *    Ayaki Sunaga             Kyoto University                  Japan         *
 *    Andreas J. Thorvaldsen   UiT The Arctic University of      Norway        *
 *    Joern Thyssen            University of Southern Denmark    Denmark       *
 *    Joost N. P. van Stralen  Vrije Universiteit Amsterdam      Netherlands   *
 *    Marta L. Vidal           Technical University of Denmark   Denmark       *
 *    Sebastien Villaume       Linkoeping University             Sweden        *
 *    Olivier Visser           University of Groningen           Netherlands   *
 *    Toke Winther             University of Southern Denmark    Denmark       *
 *    Shigeyoshi Yamamoto      Chukyo University                 Japan         *
 *                                                                             *
 *    For more information about the DIRAC code see http://diracprogram.org    *
 *                                                                             *
 *    This is an experimental code. The authors accept no responsibility       *
 *    for the performance of the code or for the correctness of the results.   *
 *                                                                             *
 *    This program is free software; you can redistribute it and/or            *
 *    modify it under the terms of the GNU Lesser General Public               *
 *    License version 2.1 as published by the Free Software Foundation.        *
 *                                                                             *
 *    If results obtained with this code are published, an                     *
 *    appropriate citation would be:                                           *
 *                                                                             *
 *    DIRAC, a relativistic ab initio electronic structure program,            *
 *    Release DIRAC21 (2021), written by                                       *
 *    T. Saue, L. Visscher, H. J. Aa. Jensen, R. Bast, and A. S. P. Gomes      *
 *    with contributions from I. A. Aucar, V. Bakken, K. G. Dyall,             *
 *    S. Dubillard, U. Ekstroem, E. Eliav, T. Enevoldsen, E. Fasshauer,        *
 *    T. Fleig, O. Fossgaard, L. Halbert, E. D. Hedegaard, T. Helgaker,        *
 *    J. Henriksson, M. Ilias, Ch. R. Jacob, S. Knecht, S. Komorovsky,         *
 *    O. Kullie, J. K. Laerdahl, C. V. Larsen, Y. S. Lee, N. H. List,          *
 *    H. S. Nataraj, M. K. Nayak, P. Norman, M. Olejniczak, J. Olsen,          *
 *    J. M. H. Olsen, A. Papadopoulos, Y. C. Park, J. K. Pedersen,             *
 *    M. Pernpointner, J. V. Pototschnig, R. Di Remigio, M. Repisky, K. Ruud,  *
 *    P. Salek, B. Schimmelpfennig, B. Senjean, A. Shee, J. Sikkema, A. Sunaga,*
 *    A. J. Thorvaldsen, J. Thyssen, J. N. P. van Stralen, M. L. Vidal,        *
 *    S. Villaume, O. Visser, T. Winther, and S. Yamamoto,                     *
 *    (see http://diracprogram.org).                                           *
 *                                                                             *
 *    as well as our reference paper: J. Chem. Phys. 152 (2020) 204104.        *
 *                                                                             *
 *******************************************************************************


Version information
-------------------

Branch        | WIP-master
Commit hash   | cc9417cbc
Commit author | Miroslav Iliaš
Commit date   | Tue Jan 18 17:07:30 2022 +0000


Configuration and build information
-----------------------------------

Who compiled             | hjj
Compiled on server       | adm-110765.pc.sdu.dk
Operating system         | Linux-4.15.0-166-generic
CMake version            | 3.22.1
CMake generator          | Unix Makefiles
CMake build type         | release
Configuration time       | 2022-01-19 09:37:10.331321
Python version           | 2.7.1
Fortran compiler         | /home/hjj/my_libs/openmpi/bin/mpif90
Fortran compiler version | 8.4.0
Fortran compiler flags   |  -g -fcray-pointer -fbacktrace -fno-range-check -DVAR_GFORTRAN -DVAR_MFDS  -fopenmp -g -fcray-pointer -fbacktrace -fno-range-check -DVAR_GFORTRAN -DVAR_MFDS  -fopenmp
C compiler               | /home/hjj/my_libs/openmpi/bin/mpicc
C compiler version       | 8.4.0
C compiler flags         |  -g  -fopenmp -g  -fopenmp
C++ compiler             | /home/hjj/my_libs/openmpi/bin/mpicxx
C++ compiler version     | 8.4.0
C++ compiler flags       |  -g -Wall -Wno-unknown-pragmas -Wno-sign-compare -Woverloaded-virtual -Wwrite-strings -Wno-unused  -fopenmp -g -Wall -Wno-unknown-pragmas -Wno-sign-compare -Woverloaded-virtual -Wwrite-strings -Wno-unused  -fopenmp
Static linking           | False
64-bit integers          | False
MPI parallelization      | True
MPI launcher             | /home/hjj/my_libs/openmpi/bin/mpiexec
Math libraries           | /usr/lib/x86_64-linux-gnu/libopenblas.so;/usr/lib/x86_64-linux-gnu/libopenblas.so
Builtin BLAS library     | OFF
Builtin LAPACK library   | OFF
Explicit libraries       | unknown
Compile definitions      | HAVE_MPI;HAVE_OPENMP;VAR_MPI;VAR_MPI2;USE_MPI_MOD_F90;SYS_LINUX;PRG_DIRAC;INSTALL_WRKMEM=64000000;BUILD_GEN1INT;HAS_PELIB;MOD_QCORR;HAS_STIELTJES;MOD_LAO_REARRANGED;MOD_MCSCF_spinfree;MOD_AOOSOC;MOD_ESR;MOD_KRCC;MOD_SRDFT;HAS_LAPLACE;MOD_EXACORR

EXACORR dependencies
--------------------
Exatensor source repo    | https://gitlab.com/DmitryLyakh/ExaTensor.git
Exatensor git hash       | e8e6f6351fabad514be9dd07d3c0a3a9e81c3f52
Exatensor configuration  | WRAP=NOWRAP BUILD_TYPE=OPT   TOOLKIT=GNU EXA_OS=LINUX GPU_CUDA=NOCUDA MPILIB=MPICH PATH_MPICH=/home/hjj/my_libs/openmpi BLASLIB=OPENBLAS PATH_BLAS_OPENBLAS=/usr/lib/x86_64-linux-gnu


 LAPACK integer*4/8 selftest passed
 Selftest of ISO_C_BINDING Fortran - C/C++ interoperability PASSED


 * openMP activated,  thread limit - # processes - # threads: *****   32    1

Execution time and host
-----------------------

 
     Date and time (Linux)  : Wed Jan 19 23:51:30 2022
     Host name              : adm-110765.pc.sdu.dk                    

 * Opening HDF5 checkpoint file 
    - DIRAC was compiled without hdf5, checkpointing is not possible


Contents of the input file
--------------------------

**DIRAC                                                                                             
.TITLE                                                                                              
  test xyz input symmetry detection                                                                 
.INPTEST                                                                                            
**MOLECULE                                                                                          
*BASIS                                                                                              
.DEFAULT                                                                                            
dyall.v2z                                                                                           
**INTEGRALS                                                                                         
*READIN                                                                                             
.UNCONTRACTED                                                                                       
*END OF                                                                                             


Contents of the molecule file
-----------------------------

4                                                                                                   
 NH3; ADF imposed C(3v) symmetry                                                                    
N      -0.00000000       0.00000000      -0.26816250                                                
H      -0.47180000      -0.81718000       0.08938750                                                
H      -0.47180000       0.81718000       0.08938750                                                
H       0.94360000      -0.00000000       0.08938750                                                


    *************************************************************************
    ******************   test xyz input symmetry detection ******************
    *************************************************************************

 Jobs in this run:
   * Input test run only


    **************************************************************************
    ************************** General DIRAC set-up **************************
    **************************************************************************

   CODATA Recommended Values of the Fundamental Physical Constants: 2018  
             Peter J. Mohr, David B. Newell and Barry N. Taylor           
         Reviews of Modern Physics, Vol. 93, 025010 (2021)                
 * The speed of light :        137.0359992
 * Running in four-component mode
 * Direct evaluation of the following two-electron integrals:
   - LL-integrals
   - SL-integrals
   - SS-integrals
   - GT-integrals
 * Spherical transformation embedded in MO-transformation
   for large components
 * Transformation to scalar RKB basis embedded in
   MO-transformation for small components
 * Thresholds for linear dependence:
   Large components:   1.00D-06
   Small components:   1.00D-08
 * General print level   :   0


    *************************************************************************
    ****************** Output from HERMIT input processing ******************
    *************************************************************************

 Default print level:        1
 Using default nuclear model: Gaussian charge distribution.

 Two-electron integrals not calculated.


 Ordinary (field-free non-relativistic) Hamiltonian integrals not calculated.



 Changes of defaults for *READIN
 -------------------------------


 Uncontracted basis forced, irrespective of basis input file.



   ***************************************************************************
   ****************** Output from MOLECULE input processing ******************
   ***************************************************************************



                     SYMADD: Detection of molecular symmetry
                     ---------------------------------------

 Symmetry test threshold:  5.00E-06

    The molecule has been centered at center of mass

 Symmetry point group found: C(3v)          

 Rotational Axes
 ---------------
       3 :      0.00000000     0.00000000     1.00000000  Isotope  0

 No unique improper rotational axes were found.

 Mirror planes
 -----------------------------
 #1 = Type: 2=h, 1=v, 0=other
 #2 = Number of atoms in plane
 #3-5 = normal vector of plane
 -----------------------------
 0     1 :      0.00000000     1.00000000     0.00000000
 0     1 :     -0.86602457     0.50000144     0.00000000
 0     1 :     -0.86602582    -0.49999928     0.00000000

 The following symmetry element was found:   Y        


  Symmetry Operations
  -------------------

  Symmetry operations: 1



                          SYMGRP:Point group information
                          ------------------------------

Full group is:  C(3v)          
Represented as: Cs 

   * The point group was generated by:

      Reflection in the xz-plane

   * Group multiplication table

        |  E   Oxz
   -----+----------
     E  |  E   Oxz
    Oxz | Oxz   E 

   * Character table

        |  E   Oxz
   -----+----------
    A'  |   1    1
    A"  |   1   -1

   * Direct product table

        | A'   A" 
   -----+----------
    A'  | A'   A" 
    A"  | A"   A' 


                            **************************
                            *** Output from DBLGRP ***
                            **************************

   * One fermion irrep:   E1 
   * Complex group. NZ = 2
   * Direct product decomposition:
          E1  x E1  : A'  + A'  + A"  + A" 


                                 Spinor structure
                                 ----------------


   * Fermion irrep no.: 1
      La  |  A' (1)  A' (1)  |
      Sa  |  A" (1)  A" (1)  |
      Lb  |  A" (4)  A" (4)  |
      Sb  |  A' (4)  A' (4)  |


                              Quaternion symmetries
                              ---------------------

    Rep  T(+)
    -----------------------------
    A'   1  i
    A"   k  j

  Nuclear repulsion energy :     11.984192242406 Hartree

  Nuclear dipole moment :    0.000000000000    0.000000000000    0.827196656506 a.u.;  origin (0,0,0)


  Atoms and basis sets
  --------------------

  Number of atom types :    2
  Total number of atoms:    4

  label    atoms   charge   prim    cont     basis   
  ----------------------------------------------------------------------
  N           1       7      34      34      L  - [10s6p1d|10s6p1d]                                              
                             85      85      S  - [6s11p6d1f|6s11p6d1f]                                          
  H           3       1       9       9      L  - [6s1p|6s1p]                                                    
                             25      25      S  - [1s6p1d|1s6p1d]                                                
  ----------------------------------------------------------------------
                             61      61      L  - large components
                            160     160      S  - small components
  ----------------------------------------------------------------------
  total:      4      10     221     221

  Cartesian basis used.
  Threshold for integrals (to be written to file):  1.00D-15


  References for the basis sets
  -----------------------------

  Atom type   1   2
   1s-3s: K.G. Dyall, Theor. Chem. Acc. (2016) 135:128                            
   4s-7s: K.G. Dyall, J. Phys. Chem. A. (2009) 113:12638.                         
   2p-3p: K.G. Dyall, Theor. Chem. Acc. (2016) 135:128                            
   4p-6p: K.G. Dyall, Theor. Chem. Acc. (1998) 99:366;                            
          revision K.G. Dyall, Theor. Chem. Acc. (2006) 115:441.                  
      7p: K.G. Dyall, Theor. Chem. Acc. (2012) 131:1172.                          
      3d: K.G. Dyall and A.S.P. Gomes, unpublished.                               
      4d: K.G. Dyall, Theor. Chem. Acc. (2007) 117:483.                           
      5d: K.G. Dyall, Theor. Chem. Acc. (2004) 112:403;                           
          revision K.G. Dyall and A.S.P. Gomes, Theor. Chem. Acc. (2009) 125:97.  


  Cartesian Coordinates (bohr)
  ----------------------------

  Total number of coordinates: 12


   1   N        x      0.0000000000
   2            y      0.0000000000
   3            z     -0.1199818071

   4   H        x     -1.7831430002
   5            y      0.0000000000
   6            z      0.5556897688

   7   H    1   x      0.8915715001
   8            y      1.5442471367
   9            z      0.5556897688

  10   H    2   x      0.8915715001
  11            y     -1.5442471367
  12            z      0.5556897688



  Cartesian coordinates in XYZ format (Angstrom)
  ----------------------------------------------

    4

N      0.0000000000   0.0000000000  -0.0634916380
H     -0.9435986395   0.0000000000   0.2940583620
H      0.4717993197   0.8171803928   0.2940583620
H      0.4717993197  -0.8171803928   0.2940583620


  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:     7    5


  Symmetry  A' ( 1)

    1   N     x    1
    2   N     z    3
    3   H     x    4
    4   H     z    6
    5   H     x    [  7  +   10 ]/2
    6   H     y    [  8  -   11 ]/2
    7   H     z    [  9  +   12 ]/2


  Symmetry  A" ( 2)

    8   N     y    2
    9   H     y    5
   10   H     x    [  7  -   10 ]/2
   11   H     y    [  8  +   11 ]/2
   12   H     z    [  9  -   12 ]/2


   Interatomic separations (in Angstroms):
   ---------------------------------------

            N           H           H  1        H  2

   N       0.000000
   H       1.009069    0.000000
   H  1    1.009069    1.634361    0.000000
   H  2    1.009069    1.634361    1.634361    0.000000




  Bond distances (angstroms):
  ---------------------------

                  atom 1     atom 2                           distance
                  ------     ------                           --------
  bond distance:    H          N                              1.009069
  bond distance:    H  1       N                              1.009069
  bond distance:    H  2       N                              1.009069


  Bond angles (degrees):
  ----------------------

                  atom 1     atom 2     atom 3                   angle
                  ------     ------     ------                   -----
  bond angle:       H  1       N          H                    108.160
  bond angle:       H  2       N          H                    108.160
  bond angle:       H  2       N          H  1                 108.160



   Nuclear repulsion energy :   11.984192242406 Hartree
   Nuclear dipole moment    :        0.00000000        3.08849427        0.82719666 a.u.

                       * Total mass:    17.026549 amu
                       * Natural abundance:  99.585 %


* Center-of-mass coordinates (a.u.):       0.000000000000000   0.000000000000000   0.000000000000000
* Center-of-mass coordinates (A)   :       0.000000000000000   0.000000000000000   0.000000000000000


                                GETLAB: AO-labels
                                -----------------

   * Large components:   22
     1  L N   1 s        2  L N   1 px       3  L N   1 py       4  L N   1 pz       5  L N   1 dxx      6  L N   1 dxy 
     7  L N   1 dxz      8  L N   1 dyy      9  L N   1 dyz     10  L N   1 dzz     11  L H   1 s       12  L H   1 px  
    13  L H   1 py      14  L H   1 pz      15  L H   1 s       16  L H   2 s       17  L H   1 px      18  L H   1 py  
    19  L H   1 pz      20  L H   2 px      21  L H   2 py      22  L H   2 pz  
   * Small components:   50
    23  S N   1 s       24  S N   1 px      25  S N   1 py      26  S N   1 pz      27  S N   1 dxx     28  S N   1 dxy 
    29  S N   1 dxz     30  S N   1 dyy     31  S N   1 dyz     32  S N   1 dzz     33  S N   1 fxxx    34  S N   1 fxxy
    35  S N   1 fxxz    36  S N   1 fxyy    37  S N   1 fxyz    38  S N   1 fxzz    39  S N   1 fyyy    40  S N   1 fyyz
    41  S N   1 fyzz    42  S N   1 fzzz    43  S H   1 s       44  S H   1 px      45  S H   1 py      46  S H   1 pz  
    47  S H   1 dxx     48  S H   1 dxy     49  S H   1 dxz     50  S H   1 dyy     51  S H   1 dyz     52  S H   1 dzz 
    53  S H   1 s       54  S H   2 s       55  S H   1 px      56  S H   1 py      57  S H   1 pz      58  S H   2 px  
    59  S H   2 py      60  S H   2 pz      61  S H   1 dxx     62  S H   1 dxy     63  S H   1 dxz     64  S H   1 dyy 
    65  S H   1 dyz     66  S H   1 dzz     67  S H   2 dxx     68  S H   2 dxy     69  S H   2 dxz     70  S H   2 dyy 
    71  S H   2 dyz     72  S H   2 dzz 


                                GETLAB: SO-labels
                                -----------------

   * Large components:   22
     1  L A' N  s        2  L A' N  px       3  L A' N  pz       4  L A' N  dxx      5  L A' N  dxz      6  L A' N  dyy 
     7  L A' N  dzz      8  L A' H  s        9  L A' H  px      10  L A' H  pz      11  L A' H  s       12  L A' H  px  
    13  L A' H  py      14  L A' H  pz      15  L A" N  py      16  L A" N  dxy     17  L A" N  dyz     18  L A" H  py  
    19  L A" H  s       20  L A" H  px      21  L A" H  py      22  L A" H  pz  
   * Small components:   50
    23  S A' N  s       24  S A' N  px      25  S A' N  pz      26  S A' N  dxx     27  S A' N  dxz     28  S A' N  dyy 
    29  S A' N  dzz     30  S A' N  fxxx    31  S A' N  fxxz    32  S A' N  fxyy    33  S A' N  fxzz    34  S A' N  fyyz
    35  S A' N  fzzz    36  S A' H  s       37  S A' H  px      38  S A' H  pz      39  S A' H  dxx     40  S A' H  dxz 
    41  S A' H  dyy     42  S A' H  dzz     43  S A' H  s       44  S A' H  px      45  S A' H  py      46  S A' H  pz  
    47  S A' H  dxx     48  S A' H  dxy     49  S A' H  dxz     50  S A' H  dyy     51  S A' H  dyz     52  S A' H  dzz 
    53  S A" N  py      54  S A" N  dxy     55  S A" N  dyz     56  S A" N  fxxy    57  S A" N  fxyz    58  S A" N  fyyy
    59  S A" N  fyzz    60  S A" H  py      61  S A" H  dxy     62  S A" H  dyz     63  S A" H  s       64  S A" H  px  
    65  S A" H  py      66  S A" H  pz      67  S A" H  dxx     68  S A" H  dxy     69  S A" H  dxz     70  S A" H  dyy 
    71  S A" H  dyz     72  S A" H  dzz 


  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:          143    78
  Number of large orbitals in each symmetry:     43    18
  Number of small orbitals in each symmetry:    100    60

* Large component functions

  Symmetry  A' ( 1)

       10 functions:    N  s   
        6 functions:    N  px  
        6 functions:    N  pz  
        1 functions:    N  dxx 
        1 functions:    N  dxz 
        1 functions:    N  dyy 
        1 functions:    N  dzz 
        6 functions:    H  s   
        1 functions:    H  px  
        1 functions:    H  pz  
        6 functions:    H  s   1+2
        1 functions:    H  px  1+2
        1 functions:    H  py  1-2
        1 functions:    H  pz  1+2

  Symmetry  A" ( 2)

        6 functions:    N  py  
        1 functions:    N  dxy 
        1 functions:    N  dyz 
        1 functions:    H  py  
        6 functions:    H  s   1-2
        1 functions:    H  px  1-2
        1 functions:    H  py  1+2
        1 functions:    H  pz  1-2

* Small component functions

  Symmetry  A' ( 1)

        6 functions:    N  s   
       11 functions:    N  px  
       11 functions:    N  pz  
        6 functions:    N  dxx 
        6 functions:    N  dxz 
        6 functions:    N  dyy 
        6 functions:    N  dzz 
        1 functions:    N  fxxx
        1 functions:    N  fxxz
        1 functions:    N  fxyy
        1 functions:    N  fxzz
        1 functions:    N  fyyz
        1 functions:    N  fzzz
        1 functions:    H  s   
        6 functions:    H  px  
        6 functions:    H  pz  
        1 functions:    H  dxx 
        1 functions:    H  dxz 
        1 functions:    H  dyy 
        1 functions:    H  dzz 
        1 functions:    H  s   1+2
        6 functions:    H  px  1+2
        6 functions:    H  py  1-2
        6 functions:    H  pz  1+2
        1 functions:    H  dxx 1+2
        1 functions:    H  dxy 1-2
        1 functions:    H  dxz 1+2
        1 functions:    H  dyy 1+2
        1 functions:    H  dyz 1-2
        1 functions:    H  dzz 1+2

  Symmetry  A" ( 2)

       11 functions:    N  py  
        6 functions:    N  dxy 
        6 functions:    N  dyz 
        1 functions:    N  fxxy
        1 functions:    N  fxyz
        1 functions:    N  fyyy
        1 functions:    N  fyzz
        6 functions:    H  py  
        1 functions:    H  dxy 
        1 functions:    H  dyz 
        1 functions:    H  s   1-2
        6 functions:    H  px  1-2
        6 functions:    H  py  1+2
        6 functions:    H  pz  1-2
        1 functions:    H  dxx 1-2
        1 functions:    H  dxy 1+2
        1 functions:    H  dxz 1-2
        1 functions:    H  dyy 1-2
        1 functions:    H  dyz 1+2
        1 functions:    H  dzz 1-2


   ***************************************************************************
   *************************** Hamiltonian defined ***************************
   ***************************************************************************


 One-electron operator origins:
 - General operator origin (a.u.)       :   0.000000000000000   0.000000000000000   0.000000000000000
 - Magnetic gauge origin (a.u.)         :   0.000000000000000   0.000000000000000   0.000000000000000
 - Dipole (and multipole) origin (a.u.) :   0.000000000000000   0.000000000000000   0.000000000000000
 * Print level:    0
 * Dirac-Coulomb Hamiltonian
 * SS integrals neglected:
   Interatomic Coulombic SS-contributions modelled by
   classical repulsion of small component atomic charges
   using tabulated charges.
 * Default integral flags passed to all modules
   - LL-integrals:     1
   - LS-integrals:     1
   - SS-integrals:     0
   - GT-integrals:     0


 Information about the restricted kinetic balance scheme:
 * Default RKB projection:
   1: Pre-projection in scalar basis
   2: Removal of unphysical solutions (via diagonalization of free particle Hamiltonian)


 ********************************************************************************
 *************************** Input consistency checks ***************************
 ********************************************************************************



    *************************************************************************
    ************************ End of input processing ************************
    *************************************************************************


*****************************************************
********** E N D   of   D I R A C  output  **********
*****************************************************


 
     Date and time (Linux)  : Wed Jan 19 23:51:30 2022
     Host name              : adm-110765.pc.sdu.dk                    

>>>> Node 0, utime: 0, stime: 0, minflt: 76911, majflt: 0, nvcsw: 69, nivcsw: 1, maxrss: 320580
>>>> Total WALL time used in DIRAC: 0s
  
  Dynamical Memory Usage Summary for Master
  
  Mean allocation size (Mb) :        48.90
  
  Largest          10  allocations
  
      488.28 Mb at subroutine __allocator_track_if_MOD_allocator_registe for test allocation of work array in DIRAC mai
        0.76 Mb at subroutine __allocator_track_if_MOD_allocator_registe for PAMINP WORK array                         
        0.02 Mb at subroutine __allocator_track_if_MOD_allocator_registe for INTADR in XPLDEF                          
        0.02 Mb at subroutine __allocator_track_if_MOD_allocator_registe for INTREP in XPLDEF                          
        0.02 Mb at subroutine __allocator_track_if_MOD_allocator_registe for INTADR in XPLDEF                          
        0.02 Mb at subroutine __allocator_track_if_MOD_allocator_registe for INTREP in XPLDEF                          
        0.02 Mb at subroutine __allocator_track_if_MOD_allocator_registe for INTADR in XPLDEF                          
        0.02 Mb at subroutine __allocator_track_if_MOD_allocator_registe for INTREP in XPLDEF                          
        0.02 Mb at subroutine __allocator_track_if_MOD_allocator_registe for INTADR in XPLDEF                          
        0.02 Mb at subroutine __allocator_track_if_MOD_allocator_registe for INTREP in XPLDEF                          
  
  Peak memory usage:       488.28 MB
  Peak memory usage:        0.477 GB
       reached at subroutine : __allocator_track_if_MOD_allocator_registe
              for variable   : test allocation of work array in DIRAC mai
  
 MEMGET high-water mark:     0.00 MB

*****************************************************
DIRAC pam run in /home/hjj/progs/gitDirac_esr/build_master_myompi/test/xyz_symmetry_recognition
